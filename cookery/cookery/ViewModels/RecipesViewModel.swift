//
//  RecipiesViewModel.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import Foundation

class RecipesViewModel: ObservableObject {
    
    @Published private(set) var recipes: [Recipe] = []
    @Published var searchResults: [Recipe] = []
    
    init() {
        recipes = Recipe.all
    }
    
    func addRecipe(recipe: Recipe)
    {
    recipes.append(recipe)
}
}

