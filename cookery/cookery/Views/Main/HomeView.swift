//
//  HomeView.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import SwiftUI

struct HomeView: View {
    
    
    @EnvironmentObject var recipesVM: RecipesViewModel
    
    @State private var searchTerm = ""
    
    private var searchedResults: [Recipe] {
        guard !searchTerm.isEmpty else { return recipesVM.recipes }
        return recipesVM.recipes.filter({ recipes in
            recipes.name.lowercased().contains(searchTerm.lowercased())
        })
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                RecipeList(recipies: searchedResults)
                    .searchable(text: $searchTerm)
            }
            .navigationTitle("My Recipe")
            
        }
        .navigationViewStyle(.stack)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .environmentObject(RecipesViewModel())
    }
}
