//
//  NewRecipeView.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import SwiftUI

struct NewRecipeView: View {
    
    @State private var showAddRecipe = false
    var body: some View {
        NavigationView {
            Button("Add Recipie manually"){
                showAddRecipe = true
                
            }
                .navigationTitle("New Recipe")
        }
        .navigationViewStyle(.stack)
        .sheet(isPresented: $showAddRecipe) {
            AddRecipeView()
        }
    }
}

struct NewRecipeView_Previews: PreviewProvider {
    static var previews: some View {
        NewRecipeView()
    }
}
