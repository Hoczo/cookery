//
//  RecipeList.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import SwiftUI

struct RecipeList: View {
    var recipies: [Recipe]
    
    var body: some View {
        VStack{
            HStack {
                Text("\(recipies.count) \(recipies.count > 1 ? "recpies" : "recipe")")
                    .font(.headline)
                    .fontWeight(.medium)
                    .opacity(0.7)
                
                Spacer()
            }
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 160), spacing: 15)], spacing: 15) {
                ForEach(recipies) { recipe in
                    NavigationLink(destination: RecipeView(recipe: recipe)) {
                        RecipeCard(recipe: recipe)
                    }
                }
            }
            .padding(.top)
            
        }
        .padding(.horizontal)
    }
        
}

struct RecipeList_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            RecipeList(recipies: Recipe.all)
        }
        
    }
}
