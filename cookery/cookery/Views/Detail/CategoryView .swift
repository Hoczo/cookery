//
//  CategoryView .swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import SwiftUI

struct CategoryView: View {
    
    @EnvironmentObject var recipesVM: RecipesViewModel
    var category: Category
    
    var recipies: [Recipe] {
        return recipesVM.recipes.filter{ $0.category == category.rawValue}
    }
    
    var body: some View {
        ScrollView {
            RecipeList(recipies: recipies)
        }
        .navigationTitle(category.rawValue + "s")
    }
}

struct CategoryView__Previews: PreviewProvider {
    static var previews: some View {
        CategoryView(category: Category.burger)
            .environmentObject(RecipesViewModel())
    }
}
