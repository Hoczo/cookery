//
//  RecipeView.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import SwiftUI

struct RecipeView: View {
    var recipe: Recipe
    
    var body: some View {
        GeometryReader { reader in
            ScrollView {
                AsyncImage(url: URL(string: recipe.image)) { image in
                    image
                        .resizable()
                        .scaledToFit()
                        .aspectRatio(contentMode: .fill)
                        .overlay(alignment: .bottom) {
                           
                        }
                    
                        
                } placeholder: {
                    Image(systemName: "photo")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 100, alignment: .center)
                        .foregroundColor(.white.opacity(0.7))
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
                .frame(width: reader.size.width, height: 300)
                .background(LinearGradient(gradient: Gradient(colors: [Color(.gray).opacity(0.3), Color(.gray)]), startPoint: .top, endPoint: .bottom))
                
                VStack(alignment: .center, spacing: 30) {
                    Text(recipe.name)
                        .font(.largeTitle)
                        .bold()
                        .multilineTextAlignment(.center)
                    Button {
                        
                        } label: {
                            Label("Add to Favourite", systemImage: "heart")
                            .labelStyle(.iconOnly)
                            .frame(alignment: .bottomTrailing)
                            .foregroundColor(.red)
                            .font(.title2)
                            .padding()
                    }
                    
                    VStack(alignment: .leading, spacing: 30) {
                        Text(recipe.description)
                        
                        VStack(alignment: .leading, spacing: 20) {
                            Text("Ingridneets")
                                .font(.headline)
                            
                            Text(recipe.ingredients)
                        }
                        
                        VStack(alignment: .leading, spacing: 20) {
                            Text("Directions")
                                .font(.headline)
                            
                            Text(recipe.directions)
                        }
                    }
                }
                .padding(.horizontal)
            }
            .ignoresSafeArea(.container, edges: .top)
        }
    }
}

struct RecipeView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeView(recipe: Recipe.all[0])
            .previewInterfaceOrientation(.portrait)
    }
}
