//
//  AddRecipeView.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import SwiftUI

struct AddRecipeView: View {
    @EnvironmentObject var recipesVM: RecipesViewModel
    
    @State private var name: String = ""
    @State private var selectedCategory: Category = Category.main
    @State private var image: String = ""
    @State private var description: String = ""
    @State private var ingrudnets: String = ""
    @State private var diretcion: String = ""
    @State private var naviagteToRecipie = false
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Name")) {
                    TextField("Recipe Name", text: $name)
                }
                
                Section(header: Text("Category")) {
                    Picker("Category", selection: $selectedCategory) {
                        ForEach(Category.allCases) {category in
                            Text(category.rawValue)
                                .tag(category)
                        }
                    }
                    .pickerStyle(.menu)
                    
                }
                
                Section(header: Text("Image")) {
                    TextField("Paste Image Url", text: $image)
                }
                
                Section(header: Text("Description")) {
                    TextEditor(text: $description)
                }
                
                Section(header: Text("Ingrudnets")) {
                    TextEditor(text: $ingrudnets)
                    
                }
                
                Section(header: Text("Diretcion")) {
                    TextEditor(text: $diretcion)
                    
                }
            }
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button {
                        dismiss()
                    } label: {
                        Label("Cancel", systemImage: "xmark")
                            .labelStyle(.iconOnly)
                    }
                }
                ToolbarItem {
                    NavigationLink(isActive: $naviagteToRecipie) {
                        RecipeView(recipe: recipesVM.recipes.sorted{ $0.dataPublished > $1.dataPublished }[0])
                            .navigationBarBackButtonHidden(true)
                    } label: {
                        Button {
                            saveRecipe()
                           naviagteToRecipie = true
                        } label: {
                            Label("Done", systemImage: "checkmark")
                                .labelStyle(.iconOnly)
                        }
                       
                    }
                    .disabled(name.isEmpty)
                }
            })
            .navigationTitle("New Recipie")
            .navigationBarTitleDisplayMode(.inline)
        }
        .navigationViewStyle(.stack)
    }
}

struct AddRecipeView_Previews: PreviewProvider {
    static var previews: some View {
        AddRecipeView()
            .environmentObject(RecipesViewModel())
    }
}


extension AddRecipeView {
    private func saveRecipe() {
        let now = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let dataPublished = dateFormatter.string(from: now)
        print(dataPublished)
        
        let recipe = Recipe(name: name, image: image, description: description, ingredients: ingrudnets, directions: diretcion, category: selectedCategory.rawValue, dataPublished: dataPublished, url: "")
        recipesVM.addRecipe(recipe: recipe)
        
    }
}
