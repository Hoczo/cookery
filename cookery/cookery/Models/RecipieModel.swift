//
//  RecipieModel.swift
//  cookery
//
//  Created by Sławek Postawa on 31/03/2022.
//

import Foundation

enum Category: String, CaseIterable, Identifiable {
    var id: String {self.rawValue}
    
    case breakfast = "Breakfast"
    case soup = "Soup"
    case salad = "Salad"
    case main = "Main"
    case side = "Side"
    case snack = "Snack"
    case drink = "Drink"
    case burger = "Burgers & Wraps"
    
}

struct Recipe: Identifiable {
    let id = UUID()
    let name: String
    let image: String
    let description: String
    let ingredients: String
    let directions: String
    let category: Category.RawValue
    let dataPublished: String
    let url: String
}

extension Recipe {
    static let all: [Recipe] = [
        Recipe(
            name: "Sweet Potato and Veggie Roll-Ups",
            image: "https://www.forksoverknives.com/wp-content/uploads/fly-images/139830/vegan-roll-ups-wordpress-1366x566-c.jpg",
            description: "Hummus and baked sweet potato add heft along with luscious texture to these vegan veggie roll-ups. You can also skip slicing and enjoy them as wraps.Note that this recipe calls for baked sweet potato. One of our favorite healthy-eating hacks is to bake a few potatoes or sweet potatoes at once and stash them in the fridge (wrapped individually in cling wrap) to use throughout the week. ",
            ingredients: "⅔ cup oil-free hummus ¼ cup sliced scallions (green onions) 4 6-inch whole wheat tortillas, 1½ cups fresh baby spinach, 1 cup chopped baked sweet potato (no skin), ¼ cup chopped red bell pepper, 2 tablespoons sunflower kernels, toasted",
            directions: "n a small bowl stir together hummus and scallions. Spread over tortillas. Top with spinach, sweet potato, bell pepper, and sunflower kernels; gently press into the hummus. Roll up tortillas and wrap individually using plastic wrap or reusable wrapper. Refrigerate at least 1 hour. Slice rolls into 1-inch pieces.",
            category: "Burgers & Wraps",
            dataPublished: "",
            url: "https://www.forksoverknives.com/recipes/vegan-burgers-wraps/sweet-potato-and-veggie-roll-ups/"
        ),
        Recipe(
            name: "Lemony Spring Pea Toasts",
            image: "https://www.forksoverknives.com/wp-content/uploads/fly-images/160043/spring-pea-mash-toasts-wordpress-1366x566-c.jpg",
            description: "These veggie-packed toasts take just 20 minutes to prepare and make for a refreshing meal any time of day. A lemony spring pea mash makes a tasty base for pickled cauliflower and thinly sliced cucumbers and radishes. When fresh spring peas are out of season or otherwise hard to find, you can substitute frozen peas in this recipe; just steam them with the edamame. For more fast-fixing mealtime inspiration, check out our favorite 39 Quick and Healthy 30-Minute Meals.",
            ingredients: "½ cup small purple or white cauliflower florets, ¼ cup slivered red onion, 2 tablespoons apple cider vinegar, 2 cups shelled fresh spring peas, ¼ cup fresh or frozen shelled edamame, thawed if frozen, 2 tablespoons chopped scallion, 2 to 3 teaspoons fresh lemon juice, 1 teaspoon chopped fresh tarragon or mint, 1 small clove garlic, minced, ¼ teaspoon hot pepper sauce, Sea salt, to taste, 2 hearty whole grain bread slices, toasted, Very thin halved slices cucumber, Very thin halved slices watermelon radishes or regular radishes, Aleppo pepper or freshly ground black pepper, to taste",
            directions: "To make pickled cauliflower, in a small saucepan combine cauliflower, red onion, vinegar, and 2 tablespoons water. Bring to boiling; remove from heCover and let stand 15 minutes; drain and set aside.Meanwhile, place peas in a steamer basket in a large saucepan. Add water to saucepan to just below basket. Bring to boiling. Steam, covered, 12 minutes or untender. Transfer peas to a colander and rinse under cold water; transfer to a bowl. Add edamame to steamer basket. Steam, covered, 2 minutes. Transfer edamto colander and rinse under cold water; drain.Add the next five ingredients (through hot pepper sauce) to the bowl with the peas. Mash mixture to combine. Season with salt. Spread pea mash on toasted bread slices. Top with edamame, cucumber, radishes, and pickled cauliflower mixture. Sprinkle with Aleppo pepper.",
            category: "Breakfast",
            dataPublished: "",
            url: "https://www.forksoverknives.com/recipes/vegan-burgers-wraps/sweet-potato-and-veggie-roll-ups/"
        ),
    ]
}
